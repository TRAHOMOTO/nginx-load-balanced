const express = require('express');
const uuid = require('uuid/v4');
const app = express();

const {NAME = 'greeter', PORT = '80', HOST = '0.0.0.0', INSTANCE = uuid() } = process.env;

app.get('/', (req, res) => {

  res.json({
    payload: 'Hello World!',
    instance: INSTANCE
  });

});

app.use((err, req, res, next) => {
  if(err) {
    console.error(err);
  }
  res.sendStatus(500);
});

const port = Number(PORT);

app.listen(port, HOST, function() {
  const addr = this.address();
  console.log(`Service "${NAME}:${INSTANCE}" listen ${addr.address}:${addr.port}`)
});
